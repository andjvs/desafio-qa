class CheckOut
  def initialize( rules=[] )
    @rules = rules
    @products = Hash.new
  end

  def total
    total = 0

    @products.each do |item, quantity|
      rules = @rules.select{ |rule| rule.product == item }.sort_by{ |r| r.quantity }.reverse!

      while quantity > 0
        rule = get_best_rule(rules, quantity)
        total += rule.price
        quantity -= rule.quantity
      end
    end
    total
  end

  def get_best_rule(rules, quantity)
    return rules.select{ |rule| rule.quantity <= quantity }.first
  end

  def scan(item)
    (@products.has_key? item) ? (@products[item] += 1) : (@products[item] = 1)
  end
end
