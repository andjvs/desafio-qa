class Rule
  attr_accessor :product, :price, :quantity

  def initialize(product, price, quantity=1)
    @product = product
    @price = price
    @quantity = quantity
  end
end
