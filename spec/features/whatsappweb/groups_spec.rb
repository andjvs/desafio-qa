require 'spec_helper'
require 'support/whatsappweb/setup.rb'
require 'page_object/whatsappweb/list.rb'

feature 'WhatsAppWeb - Managing Groups' do
  let(:list) {PageObject::WhatsAppWeb::List.new}

  context 'When user is viewing the WhatsAppWeb home screen' do

    before(:each) do
      setup_whatsapp_web(@mobile_keys)
    end

    it 'Creates a new Group' do
      list.click_on_menu
      list.click_on_new_group
      list.set_group_name( ENV['WHATSAPPWEB_GROUP_NAME'] )
      list.click_on_next
      list.find_members_to_group( ENV['WHATSAPPWEB_MEMBER1'], ENV['WHATSAPPWEB_MEMBER2'] )
      list.click_on_next

      list.expect_that_group_has_been_created
      list.expect_that_group_has_my_members( ENV['WHATSAPPWEB_MEMBER1'], ENV['WHATSAPPWEB_MEMBER2'] )
    end
  end

  def setup_whatsapp_web(mobile_keys)
    WhatsAppWeb::Setup.new(@mobile_keys)
  end
end
