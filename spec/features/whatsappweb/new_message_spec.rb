require 'spec_helper'
require 'support/whatsappweb/setup.rb'
require 'page_object/whatsappweb/chat.rb'
require 'page_object/whatsappweb/list.rb'

feature 'WhatsAppWeb - Sending a new message' do
  let(:chat) {PageObject::WhatsAppWeb::Chat.new}
  let(:list) {PageObject::WhatsAppWeb::List.new}

  context 'When user is viewing the WhatsAppWeb home screen' do

    before(:each) do
      setup_whatsapp_web(@mobile_keys)
    end

    it 'Sends a new individual message' do
      list.find_friend( ENV['WHATSAPPWEB_MY_FRIEND'] )
      list.select_friend_to_talk( ENV['WHATSAPPWEB_MY_FRIEND'] )

      chat.send_message( ENV['WHATSAPPWEB_MSG1'] )
      chat.expect_that_message_has_been_sent_now( ENV['WHATSAPPWEB_MSG1'] )
    end

    it 'Replies a message with quote of another message' do
      list.find_friend( ENV['WHATSAPPWEB_MY_FRIEND'] )
      list.select_friend_to_talk( ENV['WHATSAPPWEB_MY_FRIEND'] )

      chat.mouse_over_on_message( ENV['WHATSAPPWEB_MSG1'] )
      chat.click_on_message_options
      chat.click_on_reply
      chat.send_message( ENV['WHATSAPPWEB_MSG2'] )
      chat.expect_that_message_was_replied( ENV['WHATSAPPWEB_MSG1'] )
    end

  end

  def setup_whatsapp_web(mobile_keys)
    WhatsAppWeb::Setup.new(@mobile_keys)
  end
end
