module PageObject
  module WhatsAppWeb
    class List
      include Capybara::DSL
      include RSpec::Matchers

      def find_friend(friend)
        find(:xpath, "//input[@class='input input-search']").set( friend )
      end

      def select_friend_to_talk(friend)
        first('.chat-title', text: friend).click
      end

      def click_on_menu
        find('.icon.icon-menu').click
      end

      def click_on_new_group
        find(:xpath, "//a[@title='Novo grupo']").click
      end

      def set_group_name(name)
        find('.input.input-text').set name
      end

      def click_on_next
        find('.btn.btn-round').click
      end

      def find_members_to_group(*members)
        members.each do |member|
          find('.inputarea').set member 
          first('.chat-title', text: member).click
        end
      end

      def expect_that_group_has_been_created
        last_message=all('.message-system-body').last
        expect(last_message).to have_content 'Você criou o grupo'
      end

      def expect_that_group_has_my_members(*members)
        member_list = find('.chat-status.ellipsify')
        members.each do |member|
          expect( member_list ).to have_content member
        end
      end

    end
  end
end
