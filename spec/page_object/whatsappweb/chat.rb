module PageObject
  module WhatsAppWeb
    class Chat
      include Capybara::DSL
      include RSpec::Matchers

      def send_message(message)
        find('div#main').find('.input').set message
        find('div#main').find('.icon.btn-icon.icon-send.send-container').click
      end

      def expect_that_message_has_been_sent_now(message)
        find('.icon.btn-icon.icon-ptt') 
        #This find was necessary to unsure that message has been sent, 
        #and also because 'sleeps' is bad ideia
        expect( find('div#main') ).to have_content "#{message} ⁠⁠⁠⁠#{Time.now.strftime('%H:')}"
      end

      def mouse_over_on_message(text)
        message=find('div#main').first('.msg.msg-continuation', text: text)
        message.find('.message-text').hover
      end

      def click_on_message_options
        find('div#main').find('.js-context-icon.context-icon').click
      end

      def click_on_reply
        find(:xpath, "//a[@title='Responder']").click
      end

      def expect_that_message_was_replied(message)
        find('.icon.btn-icon.icon-ptt') 
        #This find was necessary to unsure that message has been sent, 
        #and also because 'sleeps' is bad ideia
        expect( all('.quoted-msg').last ).to have_content message
      end

    end
  end
end
