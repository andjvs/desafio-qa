require 'rspec'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'dotenv'
require 'pry-nav'

Dotenv.load

Capybara.register_driver :chrome do |app|
  Selenium::WebDriver::Chrome.driver_path = ENV['CHROMEDRIVER']
  Capybara::Selenium::Driver.new(app, :browser=> :chrome)
end
Capybara.default_driver = :chrome
